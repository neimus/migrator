//Package main program migrator.
package main

import (
	"gitlab.com/neimus/migrator/cmd"
)

func main() {
	cmd.Execute()
}
