//Package loader migration file from disk.
package loader

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/coreos/etcd/pkg/fileutil"
	"github.com/iancoleman/strcase"
	"gitlab.com/neimus/migrator/internal/converter"
	"gitlab.com/neimus/migrator/pkg/configuration"
	"gitlab.com/neimus/migrator/pkg/domain"
	"go.uber.org/zap"
)

var (
	//ErrMigrationPath - migration path is not specified or it is incorrect.
	ErrMigrationPath = errors.New("migration path is not specified or it is incorrect")
	//ErrPostfix - postfix not found for migration (.down or .up).
	ErrPostfix = errors.New("postfix not found for migration (.down or .up)")
	//ErrSeparatorNotFound - no separator found.
	ErrSeparatorNotFound = errors.New("no separator found")
	//ErrMigrationsSameName - sql migrations (down and up) must have the same name.
	ErrMigrationsSameName = errors.New("sql migrations (down and up) must have the same name")
	//ErrMigrationVersionUnique - migration version must be unique.
	ErrMigrationVersionUnique = errors.New("migration version must be unique")
	//ErrReadFile - error reading file.
	ErrReadFile = errors.New("error reading file")
	//ErrSkipFile - skip this file.
	ErrSkipFile = errors.New("skip this file")
	//ErrMigrateVersionFile - version must be greater than 0 in the migration file.
	ErrMigrateVersionFile = errors.New("version must be greater than 0 in the migration file")
)

//Loader struct.
type Loader struct {
	logger         *zap.Logger
	allowExt       string
	format         string
	listMigrations []RawMigration
	hash           map[uint64]int
}

//NewLoader construct.
func NewLoader(logger *zap.Logger) Loader {
	return Loader{logger: logger, allowExt: configuration.ExtSQL, format: configuration.FormatSQL}
}

//SetFormat - sets the migration format.
func (l *Loader) SetFormat(format string) {
	l.format = format
	switch format {
	case configuration.FormatSQL:
		l.allowExt = configuration.ExtSQL
	case configuration.FormatGolang:
		l.allowExt = configuration.ExtGolang
	}
}

//LoadMigrations - loads all migrations from disk (with filter).
func (l *Loader) LoadMigrations(ctx context.Context, filter Filter, path string, direction bool) ([]RawMigration, error) {
	l.resetMigrations()

	if !fileutil.Exist(path) {
		return nil, ErrMigrationPath
	}

	err := filepath.Walk(path, func(filePath string, info os.FileInfo, err error) error {
		select {
		case <-ctx.Done():
			return context.DeadlineExceeded
		default:
		}

		if err != nil {
			return err
		}
		//skip directory
		if info.IsDir() {
			return nil
		}

		migration, err := l.parseFile(filePath)
		if errors.Is(err, ErrSkipFile) {
			l.logger.Debug(fmt.Sprintf("skipped %s file", filePath))
			return nil
		} else if err != nil {
			return err
		}

		if filter.IsExcluded(migration) ||
			(direction && !filter.AllowUp(migration)) ||
			(!direction && !filter.AllowDown(migration)) {
			l.logger.Debug(fmt.Sprintf("%s file not loaded", filePath))
			return nil
		}

		return l.addMigration(migration)
	})
	if err != nil {
		return nil, err
	}

	if len(l.listMigrations) == 0 {
		return l.listMigrations, nil
	}

	if direction {
		sort.Sort(l)
	} else {
		sort.Sort(sort.Reverse(l))
	}

	return l.listMigrations, nil
}

func (l *Loader) addMigration(migration RawMigration) error {
	idx, ok := l.hash[migration.Version]
	if ok {
		if l.listMigrations[idx].Format == configuration.FormatGolang {
			return ErrMigrationVersionUnique
		}
		if err := l.mergeMigration(idx, migration); err != nil {
			return err
		}

		return nil
	}

	l.listMigrations = append(l.listMigrations, migration)
	l.hash[migration.Version] = len(l.listMigrations) - 1

	return nil
}

func (l *Loader) resetMigrations() {
	l.listMigrations = []RawMigration{}
	l.hash = make(map[uint64]int)
}

func (l *Loader) mergeMigration(idx int, migration RawMigration) error {
	if l.listMigrations[idx].Format == configuration.FormatSQL && l.listMigrations[idx].Name != migration.Name {
		return fmt.Errorf(
			"%w: %s and %s are different names for version %d",
			ErrMigrationsSameName,
			l.listMigrations[idx].Name,
			migration.Name, migration.Version,
		)
	}

	if l.listMigrations[idx].PathDown == "" {
		l.listMigrations[idx].PathDown = migration.PathDown
	}

	if l.listMigrations[idx].PathUp == "" {
		l.listMigrations[idx].PathUp = migration.PathUp
	}

	if l.listMigrations[idx].QueryUp == "" {
		l.listMigrations[idx].QueryUp = migration.QueryUp
	}

	if l.listMigrations[idx].QueryDown == "" {
		l.listMigrations[idx].QueryDown = migration.QueryDown
	}

	return nil
}

func (l *Loader) parseFile(path string) (RawMigration, error) {
	var (
		migration    RawMigration
		idxDirection int
		err          error
	)
	name := filepath.Base(path)

	ext := filepath.Ext(name)
	if ext != l.allowExt {
		return migration, ErrSkipFile
	}
	migration.Format = l.format

	switch migration.Format {
	case configuration.FormatGolang:
		idxDirection = strings.LastIndex(name, ext)
		migration.PathUp = path
		migration.PathDown = path
	case configuration.FormatSQL:
		query, err := ioutil.ReadFile(path)
		if err != nil {
			return migration, fmt.Errorf("%w %s", ErrReadFile, path)
		}

		if idxDirection = strings.LastIndex(name, configuration.PostfixUp); idxDirection > 0 {
			migration.PathUp = path
			migration.QueryUp = string(query)
		} else if idxDirection = strings.LastIndex(name, configuration.PostfixDown); idxDirection > 0 {
			migration.PathDown = path
			migration.QueryDown = string(query)
		} else {
			return migration, ErrPostfix
		}
	default:
		return migration, fmt.Errorf("%w %s", domain.ErrInvalidFormat, path)
	}

	idx := strings.Index(name, string(configuration.Separator))
	if idx < 0 {
		return migration, ErrSeparatorNotFound
	}

	migration.Name = converter.SanitizeMigrationName(strcase.ToLowerCamel(name[idx+1 : idxDirection]))
	migration.Version, err = converter.VersionToUint(name[:idx])
	if err != nil || migration.Version == 0 {
		return migration, fmt.Errorf("%w (%s)", ErrMigrateVersionFile, path)
	}

	return migration, nil
}

//Len is the number of elements in the collection.
func (l Loader) Len() int {
	return len(l.listMigrations)
}

//Swap swaps the elements with indexes i and j.
func (l Loader) Swap(i, j int) {
	l.listMigrations[i], l.listMigrations[j] = l.listMigrations[j], l.listMigrations[i]
	l.hash[l.listMigrations[i].Version], l.hash[l.listMigrations[j].Version] =
		l.hash[l.listMigrations[j].Version], l.hash[l.listMigrations[i].Version]
}

//Less reports whether the element with
//index i should sort before the element with index j.
func (l Loader) Less(i, j int) bool {
	return l.listMigrations[i].Version < l.listMigrations[j].Version
}
