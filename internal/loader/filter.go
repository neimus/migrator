//Package loader serves to add filtering when loading files from disk.
package loader

import "gitlab.com/neimus/migrator/pkg/domain"

//Filter struct.
type Filter struct {
	Exclude          map[uint64]domain.Migration
	Recent           domain.Migration
	RequestToVersion uint64
}

//IsExcluded - checks if migration has been added to the excluded.
func (f *Filter) IsExcluded(migration RawMigration) bool {
	exclMigration, ok := f.Exclude[migration.Version]
	if ok && exclMigration.Name == migration.Name {
		return true
	}

	return false
}

//AllowUp - checks if the migration version is allowed to apply.
func (f *Filter) AllowUp(migration RawMigration) bool {
	if f.RequestToVersion != 0 && migration.Version > f.RequestToVersion {
		return false
	} else if f.Recent.Version != 0 && migration.Version <= f.Recent.Version {
		return false
	}

	return true
}

//AllowDown - checks if the migration version is allowed to rollback.
func (f *Filter) AllowDown(migration RawMigration) bool {
	if f.RequestToVersion != 0 && migration.Version < f.RequestToVersion {
		return false
	} else if f.Recent.Version != 0 && migration.Version > f.Recent.Version {
		return false
	}

	return true
}
