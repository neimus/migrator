//Package loader - contains an entity for loaded migration from disk.
package loader

//RawMigration struct.
type RawMigration struct {
	Version   uint64
	Name      string
	PathUp    string
	PathDown  string
	Format    string
	QueryUp   string
	QueryDown string
}

//GetPath - returns the path depending on the direction.
func (rm *RawMigration) GetPath(direction bool) string {
	if direction {
		return rm.PathUp
	}

	return rm.PathDown
}

//GetQuery - returns the query depending on the direction.
func (rm *RawMigration) GetQuery(direction bool) string {
	if direction {
		return rm.QueryUp
	}

	return rm.QueryDown
}
