//Package converter provides functions for converting and sanitizing strings.
package converter

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/neimus/migrator/pkg/configuration"
)

//SanitizeMigrationName - sanitizes the migration name by removing or replacing excess characters.
func SanitizeMigrationName(name string) string {
	name = strings.Map(func(r rune) rune {
		if unicode.IsLetter(r) || unicode.IsNumber(r) {
			return r
		}

		return configuration.Separator
	}, name)
	if strings.HasSuffix(name, "test") {
		name = fmt.Sprintf("%s%s", name, string(configuration.Separator))
	}
	return name
}

//VersionToUint - converts version to number.
func VersionToUint(version string) (uint64, error) {
	return strconv.ParseUint(version, 10, 64)
}
