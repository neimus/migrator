//Package template - generates a file based on a template.
package template

//go:generate go run ./../../generate/sample.go ./../../templates/ ./

import (
	"os"
	"text/template"

	"gitlab.com/neimus/migrator/internal/loader"
	"gitlab.com/neimus/migrator/pkg/configuration"
	"gitlab.com/neimus/migrator/pkg/domain"
)

type (
	//Sample struct.
	Sample struct {
		Name string
		Text string
		Data interface{}
	}

	dataMain struct {
		Config     *configuration.Config
		Migrations []loader.RawMigration
		Direction  bool
	}

	dataGolangMigrationMethod struct {
		Version uint64
		Name    string
	}
)

func Create(path string, sample Sample) error {
	var err error
	tpl := template.New(sample.Name)
	tpl, err = tpl.Parse(sample.Text)
	if err != nil {
		return err
	}

	return writeSample(path, tpl, sample.Data)
}

func CreateMainSample(path string, config *configuration.Config, migrations []loader.RawMigration, direction bool) error {
	sample := SampleGolangMainFile
	sample.Data = dataMain{
		Config:     config,
		Migrations: migrations,
		Direction:  direction,
	}

	return Create(path, sample)
}

func CreateGolangMigrationMethod(path string, migration domain.Migration) error {
	sample := SampleGolangMigrationMethod
	sample.Data = dataGolangMigrationMethod{
		Version: migration.Version,
		Name:    migration.Name,
	}

	return Create(path, sample)
}

func writeSample(path string, tpl *template.Template, data interface{}) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	if err := tpl.Execute(file, data); err != nil {
		return err
	}

	return nil
}
