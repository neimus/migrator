# Changelog

## [0.1.1] 2021-01-31
### Fix
- fix output error
### Improvement
- rename +++NoTransaction -> +migrator.NoTransaction

## [0.0.5] 2021-01-10
### Add
- added the ability to run migrations in non-transactional mode (only for sql files)

## [0.0.4] 2020-05-21
### Improvement
- add function for deferred call

## [0.0.3] 2020-05-21
### Fix
- fixed test
- fix redo command
### Add
- add Dockerfile and docker-compose
- add integration tests
- add new command in Makefile

## [0.0.2] 2020-05-20
### Fix
- fixed logic in loader
### Improvement
- redesigned package command to run commands
### Add
- add mocks
- add tests
- add gitlab-ci

## [0.0.1] 2020-05-20
### Add
- Added command create
- Added command up
- Added command down
- Added command redo
- Added command status
- Added command version
- Added command completion

