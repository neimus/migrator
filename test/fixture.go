//Package test
package test

import (
	"path/filepath"
	"time"

	"gitlab.com/neimus/migrator/internal/loader"
	"gitlab.com/neimus/migrator/pkg/configuration"
	"gitlab.com/neimus/migrator/pkg/domain"
)

func GetMigrationByVersion(version uint64, IsApplied bool) domain.Migration {
	if version > 5 {
		return domain.Migration{}
	}

	return domain.Migration{
		Version:   version,
		Name:      GetRawMigrationByVersion(version).Name,
		IsApplied: IsApplied,
		UpdateAt:  time.Time{},
	}
}

func GetRawMigrationByVersion(version uint64) loader.RawMigration {
	return RawSQLMigrations(&configuration.Config{}, true)[version-1 : version][0]
}

func RawSQLMigrations(config *configuration.Config, direction bool) []loader.RawMigration {
	if direction {
		return []loader.RawMigration{
			{
				Version:   1,
				Name:      "testCreateFirstTable",
				PathUp:    filepath.Join(config.Path, "1_test_create_first_table.up.sql"),
				PathDown:  filepath.Join(config.Path, "1_test_create_first_table.down.sql"),
				Format:    configuration.FormatSQL,
				QueryUp:   `CREATE TABLE IF NOT EXISTS "test_first_table"();`,
				QueryDown: `DROP TABLE IF EXISTS "test_first_table";`,
			},
			{
				Version:   2,
				Name:      "testCreateSecondTable",
				PathUp:    filepath.Join(config.Path, "2_test_create_second_table.up.sql"),
				PathDown:  filepath.Join(config.Path, "2_test_create_second_table.down.sql"),
				Format:    configuration.FormatSQL,
				QueryUp:   `CREATE TABLE IF NOT EXISTS "test_second_table"();`,
				QueryDown: `DROP TABLE IF EXISTS "test_second_table";`,
			},
			{
				Version:   3,
				Name:      "testCreateThirdTable",
				PathUp:    filepath.Join(config.Path, "subfolder/3_test_create_third_table.up.sql"),
				PathDown:  filepath.Join(config.Path, "subfolder/3_test_create_third_table.down.sql"),
				Format:    configuration.FormatSQL,
				QueryUp:   `CREATE TABLE IF NOT EXISTS "test_third_table"();`,
				QueryDown: `DROP TABLE IF EXISTS "test_third_table";`,
			},
			{
				Version:   4,
				Name:      "testEmptyMigration",
				PathUp:    filepath.Join(config.Path, "4_test_empty_migration.up.sql"),
				PathDown:  filepath.Join(config.Path, "4_test_empty_migration.down.sql"),
				Format:    configuration.FormatSQL,
				QueryUp:   "",
				QueryDown: "",
			},
			{
				Version:   5,
				Name:      "testErrorMigration",
				PathUp:    filepath.Join(config.Path, "5_test_error_migration.up.sql"),
				PathDown:  filepath.Join(config.Path, "5_test_error_migration.down.sql"),
				Format:    configuration.FormatSQL,
				QueryUp:   `SELECT Bad_Migratiom FORM MORF;`,
				QueryDown: `SELECT Bad_Migratiom FORM MORF;`,
			},
		}
	}

	return []loader.RawMigration{
		{
			Version:   5,
			Name:      "testErrorMigration",
			PathUp:    filepath.Join(config.Path, "5_test_error_migration.up.sql"),
			PathDown:  filepath.Join(config.Path, "5_test_error_migration.down.sql"),
			Format:    configuration.FormatSQL,
			QueryUp:   `SELECT Bad_Migratiom FORM MORF;`,
			QueryDown: `SELECT Bad_Migratiom FORM MORF;`,
		},
		{
			Version:   4,
			Name:      "testEmptyMigration",
			PathUp:    filepath.Join(config.Path, "4_test_empty_migration.up.sql"),
			PathDown:  filepath.Join(config.Path, "4_test_empty_migration.down.sql"),
			Format:    configuration.FormatSQL,
			QueryUp:   "",
			QueryDown: "",
		},
		{
			Version:   3,
			Name:      "testCreateThirdTable",
			PathUp:    filepath.Join(config.Path, "subfolder/3_test_create_third_table.up.sql"),
			PathDown:  filepath.Join(config.Path, "subfolder/3_test_create_third_table.down.sql"),
			Format:    configuration.FormatSQL,
			QueryUp:   `CREATE TABLE IF NOT EXISTS "test_third_table"();`,
			QueryDown: `DROP TABLE IF EXISTS "test_third_table";`,
		},
		{
			Version:   2,
			Name:      "testCreateSecondTable",
			PathUp:    filepath.Join(config.Path, "2_test_create_second_table.up.sql"),
			PathDown:  filepath.Join(config.Path, "2_test_create_second_table.down.sql"),
			Format:    configuration.FormatSQL,
			QueryUp:   `CREATE TABLE IF NOT EXISTS "test_second_table"();`,
			QueryDown: `DROP TABLE IF EXISTS "test_second_table";`,
		},
		{
			Version:   1,
			Name:      "testCreateFirstTable",
			PathUp:    filepath.Join(config.Path, "1_test_create_first_table.up.sql"),
			PathDown:  filepath.Join(config.Path, "1_test_create_first_table.down.sql"),
			Format:    configuration.FormatSQL,
			QueryUp:   `CREATE TABLE IF NOT EXISTS "test_first_table"();`,
			QueryDown: `DROP TABLE IF EXISTS "test_first_table";`,
		},
	}
}

func RawGoMigrations(config *configuration.Config, direction bool) []loader.RawMigration {
	if direction {
		return []loader.RawMigration{
			{
				Version:  1,
				Name:     "testCreateFirstTable",
				PathUp:   filepath.Join(config.Path, "1_test_create_first_table.go"),
				PathDown: filepath.Join(config.Path, "1_test_create_first_table.go"),
				Format:   configuration.FormatGolang,
			},
			{
				Version:  2,
				Name:     "testCreateSecondTable",
				PathUp:   filepath.Join(config.Path, "2_test_create_second_table.go"),
				PathDown: filepath.Join(config.Path, "2_test_create_second_table.go"),
				Format:   configuration.FormatGolang,
			},
			{
				Version:  3,
				Name:     "testCreateThirdTable",
				PathUp:   filepath.Join(config.Path, "subfolder/3_test_create_third_table.go"),
				PathDown: filepath.Join(config.Path, "subfolder/3_test_create_third_table.go"),
				Format:   configuration.FormatGolang,
			},
			{
				Version:  4,
				Name:     "testEmptyMigration",
				PathUp:   filepath.Join(config.Path, "4_test_empty_migration.go"),
				PathDown: filepath.Join(config.Path, "4_test_empty_migration.go"),
				Format:   configuration.FormatGolang,
			},
			{
				Version:  5,
				Name:     "testErrorMigration",
				PathUp:   filepath.Join(config.Path, "5_test_error_migration.go"),
				PathDown: filepath.Join(config.Path, "5_test_error_migration.go"),
				Format:   configuration.FormatGolang,
			},
		}
	}

	return []loader.RawMigration{
		{
			Version:  5,
			Name:     "testErrorMigration",
			PathUp:   filepath.Join(config.Path, "5_test_error_migration.go"),
			PathDown: filepath.Join(config.Path, "5_test_error_migration.go"),
			Format:   configuration.FormatGolang,
		},
		{
			Version:  4,
			Name:     "testEmptyMigration",
			PathUp:   filepath.Join(config.Path, "4_test_empty_migration.go"),
			PathDown: filepath.Join(config.Path, "4_test_empty_migration.go"),
			Format:   configuration.FormatGolang,
		},
		{
			Version:  3,
			Name:     "testCreateThirdTable",
			PathUp:   filepath.Join(config.Path, "subfolder/3_test_create_third_table.go"),
			PathDown: filepath.Join(config.Path, "subfolder/3_test_create_third_table.go"),
			Format:   configuration.FormatGolang,
		},
		{
			Version:  2,
			Name:     "testCreateSecondTable",
			PathUp:   filepath.Join(config.Path, "2_test_create_second_table.go"),
			PathDown: filepath.Join(config.Path, "2_test_create_second_table.go"),
			Format:   configuration.FormatGolang,
		},
		{
			Version:  1,
			Name:     "testCreateFirstTable",
			PathUp:   filepath.Join(config.Path, "1_test_create_first_table.go"),
			PathDown: filepath.Join(config.Path, "1_test_create_first_table.go"),
			Format:   configuration.FormatGolang,
		},
	}
}
