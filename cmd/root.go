//Package cmd represents the base command when called without any subcommands.
package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/neimus/migrator/pkg/configuration"
	"gitlab.com/neimus/migrator/pkg/logger"
	"gitlab.com/neimus/migrator/pkg/migrate"
	"go.uber.org/zap"
)

const timeoutShutdown = 3 * time.Second

//AppVersion - application version.
const AppVersion = "0.1.2"

var (
	configFile string
	config     configuration.Config
)

// rootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{
	Use:   "migrator",
	Short: "Migration Tool",
	Long: `
Tool for working with migrations written in Go or represented as SQL files
Capabilities:
	* create - generate a migration template;
	* up - apply migration;
	* down - roll back migrations
	* redo - repetition of the last applied migration (down and up again)
	* status - displays the status of migrations in a table
	* version - output current version of migration
`,
	Version: AppVersion,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if configFile != "" {
			if err := config.ReadConfigFromFile(configFile); err != nil {
				return err
			}
		} else if config.ReadConfigFromDefaultPath() {
			fmt.Println("default configuration file loaded successfully")
		}
		config.Apply()
		return config.PathConversion()
	},
	Run: func(cmd *cobra.Command, args []string) {},
}

func init() {
	var err error
	rootCmd.PersistentFlags().StringVarP(&configFile, "config", "c", "", "path to configuration file")
	rootCmd.PersistentFlags().StringVar(&config.DSN, "dsn", "", "database connection string (Data Source Name or DSN)")
	rootCmd.PersistentFlags().StringVarP(&config.Path, "path", "p", "", "absolute path to the migration folder")

	flagFormat := "format"
	rootCmd.PersistentFlags().StringVarP(&config.Format, flagFormat, "f", "", "format of migrations (\"sql\", \"golang\")")
	err = rootCmd.RegisterFlagCompletionFunc(flagFormat, func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return []string{configuration.FormatSQL, configuration.FormatGolang}, cobra.ShellCompDirectiveDefault
	})
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	rootCmd.PersistentFlags().StringVar(&config.LogPath, "log-path", "", "absolute path to the log")

	flagLogLevel := "log-level"
	rootCmd.PersistentFlags().StringVar(&config.LogLevel, flagLogLevel, "", "logging level (\"debug\", \"info\", \"warn\", \"error\" and \"fatal\")")
	err = rootCmd.RegisterFlagCompletionFunc(flagLogLevel, func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return []string{
			configuration.LogLevelDebug,
			configuration.LogLevelInfo,
			configuration.LogLevelWarn,
			configuration.LogLevelError,
			configuration.LogLevelFatal,
		}, cobra.ShellCompDirectiveDefault
	})
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	flagLogEncoder := "log-encoder"
	rootCmd.PersistentFlags().StringVar(&config.LogEncoder, flagLogEncoder, "", "log format (\"json\", \"console\")")
	err = rootCmd.RegisterFlagCompletionFunc(flagLogEncoder, func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return []string{
			configuration.LogEncoderConsole,
			configuration.LogEncoderJSON,
		}, cobra.ShellCompDirectiveDefault
	})
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main() bold point, as linter asks :peka.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

//migrateFunc implementation of the function for working with the migrator.
type migrateFunc func(ctx context.Context, migrator migrate.Migrate, logger *zap.Logger, args ...string) error

func runMigrate(ctx context.Context, cancelFunc context.CancelFunc, migrateFunc migrateFunc, args ...string) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	zLogger, err := logger.New(&config)
	if err != nil {
		log.Fatal(err)
	}
	defer logger.Flush(zLogger)

	consoleLogger := zLogger.Named(logger.ConsoleLogger)
	migrator := migrate.NewMigrate(zLogger, &config)
	chErr := make(chan error, 1)
	go func(consoleLogger *zap.Logger, chErr chan<- error) {
		if err := migrateFunc(ctx, migrator, consoleLogger, args...); err != nil {
			chErr <- err
		}

		cancelFunc()
	}(consoleLogger, chErr)

	select {
	case <-interrupt:
		cancelFunc()
		consoleLogger.Error("program was interrupted by the user")
		timer := time.NewTimer(timeoutShutdown)
		<-timer.C
		os.Exit(15)
	case err := <-chErr:
		consoleLogger.Error(fmt.Sprintf("program terminated with an error: %s", err))
		os.Exit(1)
	case <-ctx.Done():
	}
}
