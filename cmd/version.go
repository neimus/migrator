//Package cmd - print current migration version.
package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/neimus/migrator/internal/report"
	"gitlab.com/neimus/migrator/pkg/migrate"
	"go.uber.org/zap"
)

//versionCmd represents the version command.
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print current migration version",
	Long:  `The command displays information about the latest version applied.`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx, cancelFunc := context.WithCancel(context.Background())
		runMigrate(ctx, cancelFunc, Version)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}

//Version - print current migration version.
func Version(ctx context.Context, migrator migrate.Migrate, logger *zap.Logger, args ...string) error {
	migration, err := migrator.MigrateVersion(ctx)
	if err != nil {
		return fmt.Errorf("failed to get latest migration version: %w", err)
	}

	if migration == nil {
		logger.Warn("no migration applied")
		return nil
	}
	report.PrintMigration(*migration)
	return nil
}
