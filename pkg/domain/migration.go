//Package domain objects and entities
package domain

import (
	"time"
)

//Migration entity.
type Migration struct {
	Version   uint64    `json:"version"`
	Name      string    `json:"name"`
	IsApplied bool      `json:"is_applied"`
	UpdateAt  time.Time `json:"update_at"`
}
