//Package domain errors
package domain

import "errors"

var (
	/** common errors */

	//ErrConnection - unable to connect to database.
	ErrConnection = errors.New("unable to connect to database")
	//ErrInvalidFormat - invalid migration format specified.
	ErrInvalidFormat = errors.New("invalid migration format specified")

	/** errors when creating migrations */

	//ErrMigrationFileExists - migration file already exists.
	ErrMigrationFileExists = errors.New("migration file already exists")
	//ErrCreateMigrationFile - failed to create migration file.
	ErrCreateMigrationFile = errors.New("failed to create migration file")
	//ErrMigrationNameRequired - migration name is required.
	ErrMigrationNameRequired = errors.New("migration name is required")

	/** errors during migration up/down */

	//ErrMigrateVersionIncorrect - migration version must be greater than zero.
	ErrMigrateVersionIncorrect = errors.New("migration version must be greater than zero")
	//ErrTransactionCancel - transaction cancellation error.
	ErrTransactionCancel = errors.New("transaction cancellation error")
	//ErrApplyingMigration - error applying migration.
	ErrApplyingMigration = errors.New("error applying migration")
	//ErrParallelApp - application of migrations is running in parallel.
	ErrParallelApp = errors.New(`the response time from the database has expired, 
it is possible that the application of migrations is running in parallel`)
	//ErrGetRecentMigration - failed to get the recent migration version.
	ErrGetRecentMigration = errors.New("failed to get the recent migration version")
	//ErrLoadMigrations - failed to load migrations from disk.
	ErrLoadMigrations = errors.New("failed to load migrations from disk")
	//ErrBuildProgramForMigrations - error while building the program for migrations.
	ErrBuildProgramForMigrations = errors.New("error while building the program for migrations")
	//ErrStartingProgramForMigrations - an error occurred while starting the program for migrations.
	ErrStartingProgramForMigrations = errors.New("an error occurred while starting the program for migrations")
)
