// Package configuration - loads configuration for migrator.
package configuration

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	"github.com/coreos/etcd/pkg/fileutil"
	"github.com/spf13/viper"
)

const (
	defaultConfigPath = "config/config.yml"

	//LogLevelDebug - debug logging level.
	LogLevelDebug = "debug"
	//LogLevelInfo - info logging level.
	LogLevelInfo = "info"
	//LogLevelWarn - warn logging level.
	LogLevelWarn = "warn"
	//LogLevelError - error logging level.
	LogLevelError = "error"
	//LogLevelFatal - fatal logging level.
	LogLevelFatal = "fatal"

	//LogEncoderJSON - JSON log encoder.
	LogEncoderJSON = "json"
	//LogEncoderConsole - console log encoder.
	LogEncoderConsole = "console"

	//FormatSQL - sql migration format.
	FormatSQL = "sql"
	//FormatGolang - golang migration format.
	FormatGolang = "golang"

	//ExtSQL - sql file extension.
	ExtSQL = ".sql"
	//ExtGolang - golang file extension.
	ExtGolang = ".go"

	//PostfixUp up postfix for sql files.
	PostfixUp = ".up"
	//PostfixDown down postfix for sql files.
	PostfixDown = ".down"

	//Separator - separator for migration.
	Separator = '_'
)

//ErrConfigurationFileNotFound - configuration file not found.
var ErrConfigurationFileNotFound = errors.New("configuration file not found")

//Config struct.
type Config struct {
	LogDevelopment bool
	AllowParallel  bool
	DSN            string
	Path           string
	Format         string
	LogPath        string
	LogLevel       string
	LogEncoder     string
	viperConfig    *viper.Viper
}

//ReadConfigFromFile - reads a configuration file.
func (c *Config) ReadConfigFromFile(path string) error {
	if !fileutil.Exist(path) {
		return ErrConfigurationFileNotFound
	}

	c.viper().SetConfigFile(path)
	c.viper().SetConfigType("yml")
	if err := c.viper().ReadInConfig(); err != nil {
		return err
	}

	return nil
}

//ReadConfigWithEnv - reading a configuration file using an environment variable.
func (c *Config) ReadConfigWithEnv() bool {
	configPath, ok := os.LookupEnv("MIGRATOR_CONFIG_PATH")
	if ok {
		if err := c.ReadConfigFromFile(configPath); err == nil && c.viper().IsSet("migrator") {
			return true
		}
	}

	return false
}

//ReadConfigFromDefaultPath - reads the configuration file in the default path `config/config.yml`
//relative to the program being launched.
func (c *Config) ReadConfigFromDefaultPath() bool {
	dir, err := os.Getwd()
	if err == nil {
		if err := c.ReadConfigFromFile(filepath.Join(dir, defaultConfigPath)); err == nil && c.viper().IsSet("migrator") {
			return true
		}
	}

	return false
}

//Apply - applies the read configuration to the current object.
func (c *Config) Apply() {
	if c.DSN == "" {
		c.DSN = os.ExpandEnv(c.viper().GetString("migrator.dsn"))
	}
	if c.Path == "" {
		c.Path = os.ExpandEnv(c.viper().GetString("migrator.path"))
	}
	if c.Format == "" {
		c.Format = os.ExpandEnv(c.viper().GetString("migrator.format"))
	}
	if c.LogPath == "" {
		c.LogPath = os.ExpandEnv(c.viper().GetString("migrator.log.path"))
	}
	if c.LogLevel == "" {
		c.LogLevel = os.ExpandEnv(c.viper().GetString("migrator.log.level"))
	}
	if c.LogEncoder == "" {
		c.LogEncoder = os.ExpandEnv(c.viper().GetString("migrator.log.encoder"))
	}
	c.LogDevelopment = c.viper().GetBool("migrator.log.development")
	c.AllowParallel = c.viper().GetBool("migrator.allow_parallel")
}

//PathConversion - replaces relative paths with absolute.
func (c *Config) PathConversion() error {
	var err error
	if c.Path != "" {
		c.Path, err = filepath.Abs(c.Path)
		if err != nil {
			return err
		}
	}
	if c.LogPath != "" {
		c.LogPath, err = filepath.Abs(c.LogPath)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Config) viper() *viper.Viper {
	if c.viperConfig == nil {
		c.viperConfig = viper.New()
		replacer := strings.NewReplacer(".", "_")
		c.viper().SetEnvKeyReplacer(replacer)
		c.viperConfig.AutomaticEnv() // read in environment variables that match
		c.applyDefault()
	}

	return c.viperConfig
}

func (c *Config) applyDefault() {
	c.viper().SetDefault("migrator.log.encoder", LogEncoderConsole)
	c.viper().SetDefault("migrator.log.development", true)
	c.viper().SetDefault("migrator.format", FormatSQL)
	c.viper().SetDefault("migrator.allow_parallel", false)
}
