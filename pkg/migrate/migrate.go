//Package migrate - public methods providing access to migration management
package migrate

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
	"gitlab.com/neimus/migrator/internal/command"
	"gitlab.com/neimus/migrator/internal/core"
	"gitlab.com/neimus/migrator/internal/storage"
	"gitlab.com/neimus/migrator/pkg/configuration"
	"gitlab.com/neimus/migrator/pkg/domain"
	"gitlab.com/neimus/migrator/pkg/logger"
	"go.uber.org/zap"
)

const (
	//MigrationUp - migration direction up.
	MigrationUp = true
	//MigrationDown migration direction down.
	MigrationDown = false
)

//CustomMigrateFunc - custom function for migrations.
type CustomMigrateFunc func(ctx context.Context, tx pgx.Tx) error

//Migrate struct.
type Migrate interface {
	Create(name string) error
	Status(ctx context.Context) ([]domain.Migration, error)
	Up(ctx context.Context, requestToVersion uint64) (int, error)
	DownAll(ctx context.Context) (int, error)
	Down(ctx context.Context, requestToVersion uint64) (int, error)
	Redo(ctx context.Context) (*domain.Migration, error)
	RunMigrationWithCustomFunc(ctx context.Context,
		migrateFunc CustomMigrateFunc, name string, version uint64, direction bool) error
	MigrateVersion(ctx context.Context) (*domain.Migration, error)
}

type migrate struct {
	migrateCore *core.MigrateCore
	logger      *zap.Logger
	config      *configuration.Config
}

//NewMigrate construct.
func NewMigrate(zLogger *zap.Logger, config *configuration.Config) Migrate {
	migrateStorage := storage.NewStorage(zLogger, config)
	return &migrate{
		migrateCore: core.NewMigrateCore(migrateStorage, command.NewCommand(), zLogger, config),
		logger:      zLogger.Named(logger.ConsoleLogger),
		config:      config,
	}
}

//Create a migration file.
//Creates migration files with the installed version (timestamped) and name in directory
//For the format 'sql', two files with up/down postfixes are created,
//and for the 'go' format a go-file with 'Up*/Down*'' methods is generated.
func (m *migrate) Create(name string) error {
	//todo add the ability to select the type of version (for example, human version 20200521200000)
	version := uint64(time.Now().Unix())
	return m.migrateCore.CreateMigrationFile(name, version)
}

//Up - apply all or N up migrations
//Applies all migrations since the last applied migration.
//You can specify which version to start applying migrations from
//(the version is a starting point and may not exist)
//Depending on the format of the migrations, she can run the SQL file herself
//or build a program (golang) for executing and applying migrations
//
//If parallel migration start is allowed in the settings, then parallel migrations are possible.
//Attention, while the consistency of the database may suffer!
func (m *migrate) Up(ctx context.Context, requestToVersion uint64) (int, error) {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return 0, err
	}
	defer closeFunc()

	if err := m.migrateCore.AcquireLock(ctx); err != nil {
		return 0, err
	}
	defer m.migrateCore.ReleaseLock(ctx)

	neededMigrations, err := m.migrateCore.LoadMigrations(ctx, requestToVersion, MigrationUp)
	if err != nil {
		return 0, err
	}

	if len(neededMigrations) == 0 {
		return 0, nil
	}

	return m.migrateCore.StartMigrate(ctx, neededMigrations, MigrationUp)
}

//Down - roll back all migrations
//Roll back all migrations since the last applied migration.
//Depending on the format of the migrations, she can run the SQL file herself
//or build a program (golang) for executing and applying migrations
//
//If parallel migration start is allowed in the settings, then parallel migrations are possible.
//Attention, while the consistency of the database may suffer!
func (m *migrate) DownAll(ctx context.Context) (int, error) {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return 0, err
	}
	defer closeFunc()

	if err := m.migrateCore.AcquireLock(ctx); err != nil {
		return 0, err
	}
	defer m.migrateCore.ReleaseLock(ctx)

	neededMigrations, err := m.migrateCore.LoadMigrations(ctx, 0, MigrationDown)
	if err != nil {
		return 0, err
	}

	if len(neededMigrations) == 0 {
		return 0, nil
	}

	return m.migrateCore.StartMigrate(ctx, neededMigrations, MigrationDown)
}

//Down - roll back of one or N down migrations
//Roll back of one migration since the last applied migration.
//You can specify which version to start roll back migrations from
//(the version is a starting point and may not exist)
//Depending on the format of the migrations, she can run the SQL file herself
//or build a program (golang) for executing and applying migrations
//
//If parallel migration start is allowed in the settings, then parallel migrations are possible.
//Attention, while the consistency of the database may suffer!
func (m *migrate) Down(ctx context.Context, requestToVersion uint64) (int, error) {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return 0, err
	}
	defer closeFunc()

	if err := m.migrateCore.AcquireLock(ctx); err != nil {
		return 0, err
	}
	defer m.migrateCore.ReleaseLock(ctx)

	if requestToVersion == 0 {
		migration, err := m.migrateCore.GetRecentMigration(ctx)
		if err != nil || migration == nil {
			return 0, err
		}
		requestToVersion = migration.Version
	}

	neededMigrations, err := m.migrateCore.LoadMigrations(ctx, requestToVersion, MigrationDown)
	if err != nil {
		return 0, err
	}

	if len(neededMigrations) == 0 {
		return 0, nil
	}

	return m.migrateCore.StartMigrate(ctx, neededMigrations, MigrationDown)
}

//Redo - rolls back the last applied migration and applies it again.
func (m *migrate) Redo(ctx context.Context) (*domain.Migration, error) {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return nil, err
	}
	defer closeFunc()

	if err := m.migrateCore.AcquireLock(ctx); err != nil {
		return nil, err
	}
	defer m.migrateCore.ReleaseLock(ctx)

	migration, err := m.migrateCore.GetRecentMigration(ctx)
	if err != nil || migration == nil {
		return nil, err
	}

	neededMigrations, err := m.migrateCore.LoadMigrations(ctx, migration.Version, MigrationDown)
	if err != nil {
		return migration, err
	}
	if len(neededMigrations) == 0 {
		return migration, nil
	}

	recentMigrations := neededMigrations[len(neededMigrations)-1:]

	affectedMigration, err := m.migrateCore.StartMigrate(ctx, recentMigrations, MigrationDown)
	if err != nil {
		return migration, err
	}
	if affectedMigration == 0 {
		return migration, fmt.Errorf("failed to roll back migration with version %d", migration.Version)
	}

	affectedMigration, err = m.migrateCore.StartMigrate(ctx, recentMigrations, MigrationUp)
	if err != nil {
		return migration, err
	}
	if affectedMigration == 0 {
		return migration, fmt.Errorf("failed to up migration with version %d", migration.Version)
	}

	return migration, nil
}

//MigrateVersion return information about the latest version applied.
func (m *migrate) MigrateVersion(ctx context.Context) (*domain.Migration, error) {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return nil, err
	}
	defer closeFunc()
	if err := m.migrateCore.AcquireLock(ctx); err != nil {
		return nil, err
	}
	defer m.migrateCore.ReleaseLock(ctx)

	migration, err := m.migrateCore.GetRecentMigration(ctx)
	if err != nil || migration == nil {
		return nil, err
	}

	return migration, err
}

//Status - return status of all migrations.
//Data is taken from the migration table and contains the following fields:
//Version 	- migration version (may contain only numbers)
//Name 		- human-readable name of migration
//IsApplied - migration status (applied or not applied)
//UpdateAt 	- Last update date at which any actions on migration were performed (for example, up, down, redo).
func (m *migrate) Status(ctx context.Context) ([]domain.Migration, error) {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return nil, err
	}
	defer closeFunc()
	if err := m.migrateCore.AcquireLock(ctx); err != nil {
		return nil, err
	}
	defer m.migrateCore.ReleaseLock(ctx)

	return m.migrateCore.GetMigrations(ctx)
}

//RunMigrationWithCustomFunc - starts migration with custom function
//!!attention does not use migration locks; database consistency may be impaired!!
func (m *migrate) RunMigrationWithCustomFunc(
	ctx context.Context,
	migrateFunc CustomMigrateFunc,
	name string,
	version uint64,
	direction bool,
) error {
	closeFunc, err := m.migrateCore.ConnectDB(ctx)
	if err != nil {
		return err
	}
	defer closeFunc()
	tx, err := m.migrateCore.CreateTransactionalMigration(ctx, domain.Migration{Version: version, Name: name}, direction)
	if err != nil {
		if m.config.AllowParallel && errors.Is(err, domain.ErrParallelApp) {
			return nil
		}

		if errors.Is(err, storage.ErrQueryNoAffectRows) {
			return nil
		}

		return err
	}
	sDirection := "Down"
	if direction {
		sDirection = "Up"
	}

	m.logger.Info(fmt.Sprintf("running %s migration with version %d (%s) ...", name, version, sDirection))

	return migrateFunc(ctx, tx)
}
