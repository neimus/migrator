package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/neimus/migrator/pkg/configuration"
	"gitlab.com/neimus/migrator/pkg/logger"
	"gitlab.com/neimus/migrator/pkg/migrate"
)

func main() {
	ctx := context.Background()
	config := configuration.Config{
		LogDevelopment: true,
		AllowParallel:  false,
		DSN:            "postgres://postgres:postgres@localhost:5432/migration?sslmode=disable",
		Path:           "/project/database",
		Format:         "sql",
		LogPath:        "/tmp/logs/migrator.log",
		LogLevel:       "info",
		LogEncoder:     "json",
	}

	migrateLogger, err := logger.New(&config)
	if err != nil {
		log.Fatal(err)
	}
	defer logger.Flush(migrateLogger)

	migrator := migrate.NewMigrate(migrateLogger, &config)

	//Create
	if err := migrator.Create("test migration"); err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}

	//Up
	countUp, err := migrator.Up(ctx, 0)
	if err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}
	fmt.Println(countUp)

	//Status
	migrations, err := migrator.Status(ctx)
	if err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}
	fmt.Println(migrations)

	//MigrateVersion
	recentMigration, err := migrator.MigrateVersion(ctx)
	if err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}
	fmt.Println(recentMigration)

	//Redo
	redoMigration, err := migrator.Redo(ctx)
	if err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}
	fmt.Println(redoMigration)

	//Down
	countDown, err := migrator.Down(ctx, recentMigration.Version)
	if err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}
	fmt.Println(countDown)

	//DownAll
	countDownAll, err := migrator.DownAll(ctx)
	if err != nil {
		migrateLogger.Error(err.Error())
		os.Exit(2)
	}
	fmt.Println(countDownAll)
}
