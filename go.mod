module gitlab.com/neimus/migrator

go 1.15

require (
	github.com/alexeyco/simpletable v0.0.0-20200203113705-55bd62a5b8df
	github.com/coreos/etcd v3.3.13+incompatible
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/jackc/pgconn v1.5.0
	github.com/jackc/pgx/v4 v4.6.0
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.5.1
	go.uber.org/zap v1.10.0
)
